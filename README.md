# The Anagram Maker
Welcome to ByLight's programing challenge. This challenge is the anagram maker problem. Detailed instructions are listed below. 

Remember, we are interested in your code as well as your tests, documentation, algorithms, and the process you went through to solve this challenge. So please do not be discouraged if you don't have time to complete every task.

Try to complete as many tasks as possible. Please try to spend no more than three hours on this task.

## Instructions
To get started, please [fork the repository] (https://gitlab.com/littlelingo/ruby-coding-exercise/fork).

## The Setup
You have been provided a file containing almost a million English words. Some of these words are anagrams. Your job is to code a series of tasks for finding, manipulating, and reporting on these anagrams.

To refresh your memory, the definition of an anagram is provided courtesy of the Oxford English Dictionary:

> noun. noun. /ˈænəˌɡræm/  
> a word or phrase that is made by arranging the letters of another word 
> or phrase in a different order An anagram of “Elvis” is “lives.”`

## The Tasks
Please try to complete as many of these requirements as possible. It is preferable to complete some features with workable tests rather than complete all the features with no tests or documentation.

### Goal 1 : Basic functionality
 * Find all the anagrams for words of 4 or more characters in the file provided

### Goal 2 : Getting fancy
 * Find the word that has the most anagrams
     * print each anangram
     * print the number of total anagrams found for that word
 * Given a random word, display all the anagrams it has, otherwise return nothing

### Goal 3 : By the numbers
 * Print statistics on the anagrams you've found
     * largest word that has an anagram
     * percentage of words that have anagrams
     * average length of a word that has an anagram

### Goal 4 : The dirty work
 * Write test cases for all of your code using rspec
 * Properly document each class

### Bonus : Overachiever
 * Read the file into a database to make searching for anagrams easier

## Completion
Once you have completed your work please make sure to check off what tasks you have accomlished and document your code for easy review. Good luck!

## Evaluation Criteria
Here's what we're looking for:

 * Code quality
 * Communication
 * Ruby chops
 * Feature design
 * Test quality

## FAQ
If you have another project that you think would better exemplify your skills, we'd love to see that! Regardless of the project, we will use the Evaluation Criteria.

If this is the case, please reach out.